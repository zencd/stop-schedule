<?php

include_once 'Utils.class.php';

/**
 * Структура события в расписании.
 */
class Event
{
    public $day; // like "2018-12-30"
    public $time; // like "14:00"
    public $title;
    public $subtitle;
    public $description;
    public $backgroundImg;
    public $date;
    public $date_title; // like "СРЕДА, 28 НОЯБРЯ, 18:00"
    public $timestamp; // int

    public function __construct($csv_row)
    {
        $this->day = Utils::getSafeStrFromArray($csv_row, 0);
        $this->time = Utils::getSafeStrFromArray($csv_row, 1);
        $this->title = Utils::getSafeStrFromArray($csv_row, 2);
        $this->subtitle = Utils::getSafeStrFromArray($csv_row, 3);
        $this->description = Utils::getSafeStrFromArray($csv_row, 4);
        $this->backgroundImg = Utils::getSafeStrFromArray($csv_row, 5);

        $dt = Event::parseDateTime($this->day, $this->time);

        $this->date = $dt;
        $this->timestamp = $dt->getTimestamp();
        $this->date_title = Event::makeDateTitle($dt);
    }

    private static function parseDateTime($dayStr, $timeStr)
    {
        $day_and_time = "$dayStr $timeStr";
        $dt = DateTime::createFromFormat('Y-m-d H:i', $day_and_time,
            new DateTimeZone(TZ_STR));
        if ( ! $dt) {
            $dt = Utils::makeLocalNow(); // incorrect input fallback
        }

        return $dt;
    }

    private static function makeDateTitle(DateTime &$date)
    {
        $mon = Utils::getMonthRussian($date);
        $dow = Utils::getDowRussian($date);
        $dom = date("d", $date->getTimestamp());
        $timeStr = date("H:i", $date->getTimestamp());

        return "$dow, $dom $mon, $timeStr";
    }

    /**
     * Прошёл ли эфир (или ещё нет).
     *
     * @return bool
     */
    public function isPassed()
    {
        $diff_sec = Utils::makeLocalNow()->getTimestamp() - $this->timestamp;
        $diff_hours = $diff_sec / 3600;

        return $diff_hours >= EVENT_LENGTH_HOURS;
    }

}
