<?php

include_once 'settings.inc';
include_once 'Event.class.php';
include_once 'Utils.class.php';

/**
 * Контракт генератора контента.
 */
interface ContentGenerator
{
    public function generate();
}

/**
 * Генератор расписания эфиров, HTML-текст.
 */
class ScheduleGenerator implements ContentGenerator
{
    public function generate()
    {
        $upcomingEvents = array();
        $passedEvents = array();
        $this->loadSchedule($upcomingEvents, $passedEvents);

        $context = array(
            'upcoming_events' => $upcomingEvents,
            'passed_events'   => $passedEvents,
        );
        $tplRendered = Utils::renderPhpTemplate(HTML_TPL_FILE, $context);

        //$tplRendered = time() . "\n". $tplRendered; // debug

        return $tplRendered;
    }

    private function loadSchedule(array &$upcomingEvents, array &$passedEvents)
    {
        //$file = fopen('schedule.cache.csv', 'r'); // debug
        $file = fopen($this->makeQueryUrl(), 'r');
        $cnt = 0;
        while (($csvLine = fgetcsv($file)) !== false) {
            if ($cnt > 0) {
                $event = new Event($csvLine);
                if ($event->isPassed()) {
                    array_push($passedEvents, $event);
                } else {
                    array_push($upcomingEvents, $event);
                }
            }
            $cnt += 1;
        }
        fclose($file);
    }

    private function makeQueryUrl()
    {
        $ed = $this->getEarliestDay();
        $query = "select A,B,C,D,E,F where A >= date '$ed' order by A, B";

        //echo "query: $query\n"; // debug
        return Utils::visApiUrl(DOC_ID, $query, 0);
    }

    /**
     * Возвращает самый ранний день, начиная с которого следует отображать архивные эфиры.
     *
     * @return string пример: "2018-01-01"
     */
    private function getEarliestDay()
    {
        $date = Utils::makeLocalNow();
        $date->modify('-'.DAYS_TO_KEEP_EVENT_IN_SCHEDULE.' day'); // roll some days backward

        return $date->format('Y-m-d');
    }

}
