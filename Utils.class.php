<?php

/**
 * Абстрактные утилиты, не привязанные к конкретному проекту.
 */
class Utils
{

    /**
     * Текущее время с локальной таймзоной.
     *
     * @return DateTime
     */
    public static function makeLocalNow()
    {
        return new DateTime("now", new DateTimeZone(TZ_STR));
    }

    public static function getDowRussian(DateTime &$datetime)
    {
        $titles = [
            'Воскресенье',
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
        ];
        $dow = date("w", $datetime->getTimestamp());

        return $titles[$dow];
    }

    public static function getMonthRussian(DateTime &$datetime)
    {
        $titles = array(
            null,
            'Января',
            'Февраля',
            'Марта',
            'Апреля',
            'Мая',
            'Июня',
            'Июля',
            'Августа',
            'Сентября',
            'Октября',
            'Ноября',
            'Декабря',
        );
        $mon = date("n", $datetime->getTimestamp());

        return $titles[$mon];
    }

    public static function getSafeStrFromArray(&$arr, $index)
    {
        if ($index >= count($arr)) {
            return '';
        }

        return trim($arr[$index]);
    }

    public static function renderPhpTemplate($templatePath, array &$args)
    {
        ob_start();
        include($templatePath);
        $var = ob_get_contents();
        ob_end_clean();

        return $var;
    }

    /**
     * Создаёт урл для обращения к The Google Visualization API Query Language.
     *
     * @param $docId string UID гуглотаблицы
     * @param $query string "select A"
     * @param $sheetId int ID листа внутри таблицы; обычно 0 для первой страницы
     *
     * @return string URL
     */
    public static function visApiUrl($docId, $query, $sheetId) {
        return "https://docs.google.com/spreadsheets/d/$docId/gviz/tq?gid=$sheetId&tqx=out:csv&tq=" . rawurlencode($query);
    }

}