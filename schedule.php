<?php

include_once 'settings.inc';
include_once 'ScheduleGenerator.class.php';
include_once 'CachingWebServer.class.php';


function main()
{
    // php 5.6 requires it; although maybe useful
    date_default_timezone_set(TZ_STR);

    $scheduleGenerator = new ScheduleGenerator();

    $webServer = new CachingWebServer(
        $scheduleGenerator,
        HTML_CACHE_FILE,
        REGEN_MINUTES * 60);
    $webServer->serve();
}

main();
