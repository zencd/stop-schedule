<?php

// то что имеет смысла настроить руками

/**
 * ID документа google sheets, где ведётся расписание
 * документ должен быть доступен на чтение анонимно по урлу
 */
const DOC_ID = '1R8Obb6ihtbj2Q4A4hRV730ufo2MOeka0Yyyy7DIs3a4';

/**
 * через столько времени расписание пересоздаётся из google sheets
 * в остальное время отдаётся закэшированная версия
 * ед. изм.: минуты
 */
//const REGEN_MINUTES = 0; // debug
const REGEN_MINUTES = 60;

/**
 * эфир считается прошедшим если после его начала прошло столько времени
 * ед. изм.: часы
 */
const EVENT_LENGTH_HOURS = 4;

/**
 * сколько дней прошедший эфир продолжает отображаться в расписании (в подвале)
 * ед. изм.: сутки
 */
const DAYS_TO_KEEP_EVENT_IN_SCHEDULE = 30;

// далее то что НЕ имеет особого смысла настраивать

// путь к файлу HTML-шаблона; на чтение
const HTML_TPL_FILE = __DIR__.'/schedule.tpl.html';

// сгенерированная и закэшированная версия HTML; файл; чтение/запись
const HTML_CACHE_FILE = __DIR__.'/schedule.cache.html';

const TZ_STR = 'Europe/Moscow';
